package uz.pdp.swagger.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.swagger.payload.ApiResponse;
import uz.pdp.swagger.payload.RegisterDto;
import uz.pdp.swagger.payload.SignIn;
import uz.pdp.swagger.service.AuthService;


@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody SignIn signIn) {
        ApiResponse response = authService.login(signIn);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/register")
    public HttpEntity<?> register(@RequestBody RegisterDto registerDto){
        ApiResponse response = authService.registerUser(registerDto);
        return ResponseEntity.ok(response);
    }


}
