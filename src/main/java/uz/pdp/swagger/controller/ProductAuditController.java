package uz.pdp.swagger.controller;

import org.javers.core.Changes;
import org.javers.core.Javers;
import org.javers.repository.jql.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.swagger.entity.Product;

@RestController
@RequestMapping(value = "/audit")

public class ProductAuditController {

    private final Javers javers;

    @Autowired
    public ProductAuditController(Javers javers) {
        this.javers = javers;
    }

    @GetMapping("/product")
    public String getProductChanges() {
        Changes changes = javers.findChanges(QueryBuilder.byClass(Product.class).build());
        return changes.prettyPrint();

        //        QueryBuilder jqlQuery = QueryBuilder.byClass(Product.class);
//
//        List<Change> changes = javers.findChanges(jqlQuery.build());
//
//        return javers.getJsonConverter().toJson(changes);
    }
}