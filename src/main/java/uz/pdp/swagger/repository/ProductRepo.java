package uz.pdp.swagger.repository;

import org.javers.spring.annotation.JaversSpringDataAuditable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.swagger.entity.Product;
import java.util.UUID;

@RepositoryRestResource(path = "product")
@JaversSpringDataAuditable
public interface ProductRepo extends JpaRepository<Product, UUID> {
}
